<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class NewPostNotifly extends Notification
{
    use Queueable;

    public $post;

    public function __construct($post)
    {
        return $this->post = $post;
    }


    public function via($notifiable)
    {
        return ['mail'];
    }


    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->greeting('New Post Avvalible')
            ->subject('Hello , Subscriber')
            ->line('There is a new Post check it out ')
            ->line('Post Title : '.$this->post->title)
           /* ->action('View Post', url())*/
            ->line('Thank you for using our application!');
    }


    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
