<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;


    protected $redirectTo ;


    public function __construct()
    {
        //koristime od pogore funkcijata za podolu da proverime posle login sho role e usero
        //za da go redirektnime na pravata ruta sprema negovoto role :D

        if(Auth::check() && Auth::user()->role()->id==1)
        {
          $this->redirectTo = route('admin.dashboard');
        }
        else{
            $this->redirectTo = route('author.dashboard');
        }

        $this->middleware('guest')->except('logout');
    }
}
