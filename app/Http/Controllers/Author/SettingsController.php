<?php

namespace App\Http\Controllers\Author;

use App\Http\Controllers\Controller;
use App\User;
use Auth;
use Brian2694\Toastr\Facades\Toastr;
use Carbon\Carbon;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Str;

class SettingsController extends Controller
{
    public function index(){
        return view('author.settings');
    }

    public function updateProfile(Request $request){
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'image' => 'required',
        ]);

        $image=$request->file('image');
        $slug = Str::slug($request->name);

        $user=User::findorfail(Auth::id());

        if (isset($image)) {
            $currentDate = Carbon::now()->toDateString();
            $imageName = $slug . '-' . $currentDate . '-' . uniqid() . '.' . $image->getClientOriginalExtension();

            if (!Storage::disk('public')->exists('profile')) {//ako ne postoj kreiraj novo
                Storage::disk('public')->makeDirectory('profile');//kreiraj novo
            }
            //delete old image
            if (Storage::disk('public')->exists('profile/' . $user->image)) {
                (Storage::disk('public')->delete('profile/' . $user->image));
            }

            $profile = Image::make($image)->resize(500, 500)->stream();
            Storage::disk('public')->put('profile/' . $imageName, $profile);

        } else {
            $imageName = $user->image;
        }

        $user->name=$request->name;
        $user->email=$request->email;
        $user->image=$imageName;
        $user->about=$request->about;

        $user->save();

        Toastr::success('User Updated', 'Success');
        return redirect()->back();

    }

    public function updatePassword(Request $request){
        $this->validate($request, [
            'old_password' => 'required',
            'password' => 'required|confirmed',
        ]);

        $hashPassword=Auth::user()->password;

        if(Hash::check($request->old_password,$hashPassword)){
            if(!Hash::check($request->password,$hashPassword)){
                $user=User::find(Auth::id());
                $user->password=Hash::make($request->password);
                $user->save();
                Toastr::success('Password changed', 'Success');
                Auth::logout();
                return redirect()->back();
            }
            else{
                Toastr::error('Password cannot be the same as the old one', 'Error');
                return redirect()->back();
            }
        }else{
            Toastr::error('Passwords must match', 'Error');
            return redirect()->back();
        }
    }
}
