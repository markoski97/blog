<?php

namespace App\Http\Controllers\Author;

use App\Category;
use App\Http\Controllers\Controller;
use App\Notifications\NewAuthorPost;
use App\Post;
use App\Tag;
use App\User;
use Brian2694\Toastr\Facades\Toastr;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Str;

class PostController extends Controller
{

    public function index()
    {
        $posts=Auth::user()->posts()->latest()->get();

        return view('author.post.index',compact('posts'));
    }


    public function create()
    {
        $categories=Category::all();
        $tags=Tag::all();
        return view('author.post.create',compact('categories','tags'));
    }


    public function store(Request $request)
    {
        $this->validate($request,[
            'title'=>'required',
            'image'=>'required',
            'categories'=>'required',
            'tags'=>'required',
            'body'=>'required',
        ]);

        $image=$request->file('image');

        $slug=Str::slug($request->title);

        if(isset($image)){
            $currentDate = Carbon::now()->toDateString();
            $imageName = $slug . '-' . $currentDate . '-' . uniqid() . '.' . $image->getClientOriginalExtension();

            if (!Storage::disk('public')->exists('post')) {//ako ne postoj kreiraj novo
                Storage::disk('public')->makeDirectory('post');//kreiraj novo
            }
                $postImage = Image::make($image)->resize(1600, 1066)->stream();
                Storage::disk('public')->put('post/' . $imageName, $postImage);

        }
        else{
            $imageName='default.png';
        }

        $post=new Post();

        $post->user_id=Auth()->id();
        $post->title=$request->title;
        $post->slug=$slug;
        $post->image=$imageName;
        $post->body=$request->body;

        if(isset($request->status)){
            $post->status=true;
        }else{
            $post->status=false;
        }
        $post->is_approved=false;
        $post->save();

        $post->categories()->attach($request->categories);
        $post->tags()->attach($request->tags);

        $users=User::where('role_id','1')->get();

        Notification::send($users,new NewAuthorPost($post));


        Toastr::success('Post Created','Success');
        return redirect()->route('author.post.index');

    }

    public function show(Post $post)
    {
        if(!$post->user_id  = Auth::id())
        {
            Toastr::error('You have no autorization to access this post','error');
            return redirect()->back();
        }

        return view('author.post.show',compact('post'));
    }


    public function edit(Post $post)
    {
        if(!$post->user_id  = Auth::id())
        {
            Toastr::error('You have no autorization to access this post','error');
            return redirect()->back();
        }
        $categories=Category::all();
        $tags=Tag::all();
        return view('author.post.edit',compact('categories','tags','post'));
    }


    public function update(Request $request, Post $post)
    {
        if(!$post->user_id  = Auth::id())
        {
            Toastr::error('You have no autorization to access this post','error');
            return redirect()->back();
        }
        $this->validate($request,[
            'title'=>'required',
            'categories'=>'required',
            'tags'=>'required',
            'body'=>'required',
        ]);

        $image=$request->file('image');

        $slug=Str::slug($request->title);

        if(isset($image)){
            $currentDate = Carbon::now()->toDateString();
            $imageName = $slug . '-' . $currentDate . '-' . uniqid() . '.' . $image->getClientOriginalExtension();

            if (!Storage::disk('public')->exists('post')) {//ako ne postoj kreiraj novo
                Storage::disk('public')->makeDirectory('post');//kreiraj novo
            }
            //delete old image
            if(Storage::disk('public')->exists('post/'.$post->image)) {
                (Storage::disk('public')->delete('post/' . $post->image));

            }

            $postImage = Image::make($image)->resize(1600, 1066)->stream();
            Storage::disk('public')->put('post/' . $imageName, $postImage);

        }
        else{
            $imageName=$post->image;
        }

        $post->user_id=Auth()->id();
        $post->title=$request->title;
        $post->slug=$slug;
        $post->image=$imageName;
        $post->body=$request->body;

        if(isset($request->status)){
            $post->status=true;
        }else{
            $post->status=false;
        }
        $post->is_approved=false;
        $post->save();

        $post->categories()->sync($request->categories);
        $post->tags()->sync($request->tags);
        Toastr::success('Post Updated','Success');
        return redirect()->route('author.post.index');
    }


    public function destroy(Post $post)
    {
        if(Storage::disk('public')->exists('post/'.$post->image))
        {
            Storage::disk('public')->delete('post/'.$post->image);
        }
        $post->categories()->detach();
        $post->tags()->detach();

        $post->delete();
        Toastr::success('Post Deleted','Success');

        return redirect()->route('author.post.index');
    }
}
