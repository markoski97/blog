<?php

namespace App\Http\Controllers\Author;

use App\Comment;
use App\Http\Controllers\Controller;
use Auth;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function index(){
        $posts=Auth::user()->posts;

        return view('author.comments',compact('posts'));
    }

    public function destroy($id){
        $commnets=Comment::findorfail($id)->delete();

        if($commnets->post->user->id == Auth::id())
        {
            Toastr::success('Comment sacesfully deleted','Success');
            return redirect()->back();
        }
        else {
            Toastr::error('You have no autority to delete this comment','Error');
            return redirect()->back();
        }
    }
}
