<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use Brian2694\Toastr\Facades\Toastr;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Str;

class CategoryController extends Controller
{

    public function index()
    {
        $categories = Category::latest()->get();

        return view('admin.category.index', compact('categories'));
    }


    public function create()
    {
        return view('admin.category.create');
    }


    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:categories',
            'image' => 'required|mimes:jpg,png,jpeg,bmp'
        ]);

        $image = $request->file('image');
        $slug = Str::slug($request->name);

        if (isset($image)) {
            $currentDate = Carbon::now()->toDateString();
            $imageName = $slug . '-' . $currentDate . '-' . uniqid() . '.' . $image->getClientOriginalExtension();


            if (!Storage::disk('public')->exists('category')) {//ako ne postoj kreiraj novo
                Storage::disk('public')->makeDirectory('category');//kreiraj novo
            }
            //resize image for  category and upload
            $category = Image::make($image)->resize(1600, 479)->stream();
            Storage::disk('public')->put('category/' . $imageName, $category);


            if (!Storage::disk('public')->exists('category/slider')) {
                Storage::disk('public')->makeDirectory('category/slider');
            }
            //resize image for  category slider and upload
            $slider = Image::make($image)->resize(500, 333)->stream();
            Storage::disk('public')->put('category/slider/' . $imageName, $slider);

        }
        else{
            $imageName='default.png';
        }

        $category=new Category();

        $category->name=$request->name;
        $category->slug=Str::slug($request->name);
        $category->image=$imageName;

        Toastr::success('Category Created','Success');
        $category->save();
        return redirect()->route('admin.category.index');

    }


    public function edit($id)
    {
       $category=Category::findorfail($id);

       return view('admin.category.edit',compact('category'));
    }


    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|unique:categories',
            'image' => 'required|mimes:jpg,png,jpeg,bmp'
        ]);

        $image = $request->file('image');
        $slug = Str::slug($request->name);
        $category=Category::findorfail($id);

        if (isset($image)) {
            $currentDate = Carbon::now()->toDateString();
            $imageName = $slug . '-' . $currentDate . '-' . uniqid() . '.' . $image->getClientOriginalExtension();


            if (!Storage::disk('public')->exists('category')) {//ako ne postoj kreiraj novo
                Storage::disk('public')->makeDirectory('category');//kreiraj novo
            }
            //ako ima nov file izbrisigo stario
            if(Storage::disk('public')->exists('category/'.$category->image)){
                (Storage::disk('public')->delete('category/'.$category->image));
            }
            //resize image for  category and upload
            $categoryImage = Image::make($image)->resize(1600, 479)->stream();
            Storage::disk('public')->put('category/' . $imageName, $categoryImage);


            if (!Storage::disk('public')->exists('category/slider')) {
                Storage::disk('public')->makeDirectory('category/slider');
            }
            //izbrisigo starata slika od category slider
            if(Storage::disk('public')->exists('category/slider/'.$category->image)){
                (Storage::disk('public')->delete('category/slider/'.$category->image));
            }
            //resize image for  category slider and upload
            $slider = Image::make($image)->resize(500, 333)->stream();
            Storage::disk('public')->put('category/slider/' . $imageName, $slider);

        }
        else{
            $imageName=$category->image;
        }

        $category->name=$request->name;
        $category->slug=Str::slug($request->name);
        $category->image=$imageName;

        Toastr::success('Category Updated','Success');
        $category->save();
        return redirect()->route('admin.category.index');
    }


    public function destroy($id)
    {
        $category = Category::find($id);

        if(Storage::disk('public')->exists('category/'.$category->image))
        {
            Storage::disk('public')->delete('category/'.$category->image);
        }

        if(Storage::disk('public')->exists('category/slider/'.$category->image))
        {
            Storage::disk('public')->delete('category/slider/'.$category->image);
        }

        $category->delete();
        Toastr::success('Category Deleted','Success');

        return redirect()->route('admin.category.index');

    }
}
