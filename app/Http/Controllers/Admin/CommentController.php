<?php

namespace App\Http\Controllers\Admin;

use App\Comment;
use App\Http\Controllers\Controller;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function index(){
        $commnets=Comment::latest()->get();

        return view('admin.comments',compact('commnets'));
    }

    public function destroy($id){
        $commnets=Comment::findorfail($id)->delete();
        Toastr::success('Comment sacesfully deleted','Success');
        return redirect()->back();

    }
}
