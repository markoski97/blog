<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
  //Ovaj del e ako e vejce logiran usero da go redirektira na toj view sprema negojo role
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check() && Auth::user()->role->id == 1) {
            return redirect()->route('admin.dashboard');
        }

        elseif(Auth::guard($guard)->check() && Auth::user()->role->id == 2) {
            return redirect()->route('author.dashboard');
        }

        else{
            return $next($request);
        }
    }
}
