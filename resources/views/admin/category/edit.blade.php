@extends('layouts.backend.app')

@section('title',"Category")

@push('css')

@endpush

@section('content')


<div class="container-fluid">

    <!-- Vertical Layout -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Edit Category
                    </h2>
                </div>
                <div class="body">
                    <form action="{{route('admin.category.update',$category->id)}}" method="Post" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')

                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" id="name" class="form-control" name="name" value="{{$category->name}}" placeholder="">
                                <label class="form-label">Category Name</label>
                            </div>
                        </div>

                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="file" name="image">
                            </div>
                        </div>

                        <a class="btn btn-danger m-t-15 waves-effect"
                           href="{{route('admin.category.index')}}">
                            Back
                        </a>

                        <button type="submit" class="btn btn-primary m-t-15 waves-effect">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
    <!-- #END# Vertical Layout -->
    <!-- Vertical Layout | With Floating Label -->
@endsection
@push('js')

@endpush

