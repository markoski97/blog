<?php

use App\Category;
use Illuminate\Support\Facades\Route;


//Web Page Rutes
Route::get('/','HomeController@index')->name('home');

//prebaruvajne na postoj po kategorija
Route::get('/category/{slug}','PostController@postByCategory')->name('category.posts');
//prebaruvajne na postoj po tag
Route::get('/tag/{slug}','PostController@postByTag')->name('tag.posts');

Route::get('/post/{slug}','PostController@details')->name('post.details');
Route::get('/posts','PostController@index')->name('post.index');

Route::post('subscriber','SubscriberController@store')->name('subscriber.store');
Route::get('/search','SearchController@search')->name('search');

//probaruvajne po avtor
Route::get('profile/{username}','AuthorController@profile')->name('author.profile');


Auth::routes();
Route::group(['middleware' =>['auth']], function()
{
    Route::post('/favorite/{post}/add','FavoriteController@add')->name('post.favorite');
    Route::post('/comment/{post}','CommentController@store')->name('comment.store');
});


//admin routes
Route::group(['as' => 'admin.','prefix' => 'admin','namespace' => 'Admin','middleware' =>['auth','admin']], function()
{
    Route::get('/dashboard','DashboardController@index')->name('dashboard');

    Route::get('/settings','SettingsController@index')->name('settings');
    Route::put('/profile-update','SettingsController@updateProfile')->name('profile.update');
    Route::put('/password-update','SettingsController@updatePassword')->name('password.update');


    Route::resource('/tag','TagController');
    Route::resource('/category','CategoryController');
    Route::resource('/post','PostController');

    Route::get('/favorite','FavoriteController@index')->name('favorite.index');

    Route::get('/comments','CommentController@index')->name('comments.index');
    Route::delete('/comments/{id}','CommentController@destroy')->name('comments.destroy');

    Route::get('/authors','AuthorController@index')->name('authors.index');
    Route::delete('/authors/{id}','AuthorController@destroy')->name('authors.destroy');

    Route::get('/pending/post','PostController@pending')->name('post.pending');
    Route::get('/post/{id}/approve','PostController@approve')->name('post.approve');

    Route::get('/subscriber','SubscriberController@index')->name('subscriber.index');
    Route::delete('/subscriber/{subscriber}','SubscriberController@destroy')->name('subscriber.destroy');

});

//author routes
Route::group(['as' => 'author.','prefix' => 'author','namespace' => 'Author','middleware' =>['auth','author']], function()
{
    Route::get('/dashboard','DashboardController@index')->name('dashboard');
    Route::resource('/post','PostController');

    Route::get('/favorite','FavoriteController@index')->name('favorite.index');

    Route::get('/comments','CommentController@index')->name('comments.index');
    Route::delete('/comments/{id}','CommentController@destroy')->name('comments.destroy');

    Route::get('/settings','SettingsController@index')->name('settings');
    Route::put('/profile-update','SettingsController@updateProfile')->name('profile.update');
    Route::put('/password-update','SettingsController@updatePassword')->name('password.update');
});




